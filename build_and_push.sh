#!/bin/bash


echo -e "\n\n\n===== Building docker image =====\n"
docker build -t fakau_server .

echo -e "\n\n\n===== Pushing docker image =====\n"
docker tag fakau_server:latest eu.gcr.io/fakau-273114/fakau_server:latest
sudo docker push eu.gcr.io/fakau-273114/fakau_server:latest

echo -e "\n\n\n===== Deploying service revision =====\n"
~/Downloads/google-cloud-sdk/bin/gcloud run deploy fakauserver --image eu.gcr.io/fakau-273114/fakau_server:latest --platform=managed --region=europe-west1 --service-account=fakau-service-account@fakau-273114.iam.gserviceaccount.com

echo -e "\n\n\n"
