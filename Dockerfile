FROM python:3-alpine

RUN pip install flask

COPY app/ /app/

EXPOSE 5000/tcp

CMD ["python3", "app/app.py"]
