from src.statement import Statement
from src.card import special_cards
from src.helpers import logger

from random import randint, getrandbits, sample


class Player:
    def __init__(self, name):
        self.name = name
        self.deck = []
        self.drawn_cards = []
        self.face_down_cards = []
        self.holding_jc_count = 0

    def _take_cards(self, deck: list, num_of_cards: int, method: str):
        method_dict = {"draw": self.drawn_cards, "earn": self.deck}

        cards_taken = 0
        for i in range(num_of_cards):
            if len(deck) > 0:
                random_index = randint(0, len(deck) - 1)
                random_card = deck.pop(random_index)
                method_dict[method].append(random_card)
                cards_taken += 1

        return cards_taken

    def is_holding_jc(self):
        for card in self.drawn_cards:
            if card == special_cards.JACK_CLUBS:
                return True
        return False

    def clear_face_down_cards(self):
        self.face_down_cards = []

    def clear_drawn_cards(self):
        self.drawn_cards = []

    def draw_cards(self, deck):
        self._take_cards(deck, 2, "draw")

    def earn_cards(self, deck, num_of_cards):
        return self._take_cards(deck, num_of_cards, "earn")

    def lay_down_cards(self, num_of_cards=None):
        if num_of_cards is None:
            num_of_cards = randint(2, 5)

        for i in range(num_of_cards):
            if len(self.deck) > 0:
                card = self.deck.pop()
                card.hide()
                self.face_down_cards.append(card)

        return len(self.face_down_cards)

    def reveal_face_down_cards(self):
        for card in self.face_down_cards:
            card.unhide()

    def calc_sum_of_cards(self):
        player_sum = 0
        for card in self.face_down_cards:
            player_sum += card.value

        return player_sum

    def is_busted(self):
        return self.calc_sum_of_cards() > 20

    def cannot_make_statement(self):
        max_times = 3 if self.name == "Moti" else 2
        return self.is_holding_jc() and self.holding_jc_count == max_times


class MotiPlayer(Player):
    def __init__(self):
        super().__init__("Moti")

    @staticmethod
    def _declare_cards(player, num_of_cards, moti_declare_dict):
        possible_indexes = [str(i) for i in range(len(player.face_down_cards))]
        moti_declare_dict[player.name] = "".join(sample(possible_indexes, num_of_cards))

    @staticmethod
    def respond():
        return bool(getrandbits(1))

    def make_statement(self):
        if randint(1, 10) == 10 or self.is_holding_jc():
            self.holding_jc_count += 1
            return Statement().create_jc_statement()
        return Statement().create_random_statement()

    def get_nums_declare_take(self, player, credit_dict):
        credit = min(len(player.face_down_cards), credit_dict[f"{self.name}_{player.name}"])
        random_num_to_take = randint(0, credit)
        return {"take": random_num_to_take, "declare": credit - random_num_to_take}

    def use_credit(self, players, credit_dict):
        moti_declare_dict = dict()

        for player_name in players:
            if f"{self.name}_{player_name}" in credit_dict:
                moti_credit_use = self.get_nums_declare_take(players[player_name], credit_dict)
                logger.log(f"{self.name}'s credit over {player_name}: {moti_credit_use}")
                self._declare_cards(players[player_name], moti_credit_use["declare"], moti_declare_dict)

                self.earn_cards(players[player_name].deck, moti_credit_use["take"])
                logger.log(f"{self.name} takes {moti_credit_use['take']} from {player_name} (credit)")

        return moti_declare_dict
