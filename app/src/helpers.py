from flask import request
from time import sleep
import sys


class Logger:
    def __init__(self):
        self.logging_freq = 4

    @staticmethod
    def log(entry):
        print(f"\n{entry}\n", file=sys.stderr)


class UrlInfo:
    @staticmethod
    def get_base_url():
        base_url = request.url_root
        return base_url if "https" in base_url or "localhost" in base_url else base_url.replace('http', 'https')

    @staticmethod
    def get_param(param_name):
        args = request.args.to_dict()
        form = request.form.to_dict()
        return args[param_name] if param_name in args else form[param_name] if param_name in form else None


class Waiting:
    def __init__(self):
        self.sleep_sec = 0.2

    def wait(self):
        sleep(self.sleep_sec)

    def wait_for_event(self, bool_func):
        iteration_count = 0
        while not bool_func():
            iteration_count += 1
            self.wait()

            if iteration_count % (logger.logging_freq / self.sleep_sec) == 0 and "player" in request.args.to_dict():
                logger.log(f"{UrlInfo.get_param('player')} waiting...")


def performed_once(func):
    def wrapper_func(*args, **kwargs):
        match = kwargs["match"] if "match" in kwargs else args[0]

        if UrlInfo.get_param("player") == match.controlling_player:
            func(*args)
        match.wait_for_players()

    return wrapper_func


logger = Logger()
waiting = Waiting()
