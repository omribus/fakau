from flask import render_template, request, redirect
from src.match import Match
from src.helpers import UrlInfo, logger, waiting


class Server:
    def __init__(self):
        self.matches = dict()

    @staticmethod
    def _build_url(url):
        return UrlInfo.get_base_url() + url

    def _wait_for_statement(self):
        match_id = UrlInfo.get_param("match")
        match = self.matches[match_id]

        waiting.wait_for_event(match.phase_a.is_cards_drawn)

        stating_player = match.players[match.phase_a.player_making_statement]
        if stating_player.cannot_make_statement():
            logger.log(f"{stating_player} cannot make a statement")
            return render_template("game_over.html", **match.context_game_over(reason="cannot_state"))

        else:
            waiting.wait_for_event(match.phase_a.is_statement)
            return redirect(self._build_url(f"make_response?player={UrlInfo.get_param('player')}&match={match_id}"))

    def _new_match(self):
        num_of_players = int(UrlInfo.get_param("players"))
        new_match_id = UrlInfo.get_param("new_match_id")

        logger.log(f"Hosting NEW match with ID: {new_match_id}")
        self.matches[new_match_id] = Match(num_of_players=num_of_players)
        return new_match_id

    @staticmethod
    def _existing_match():
        existing_match_id = UrlInfo.get_param("existing_match_id")
        logger.log(f"Joining EXISTING match with ID: {existing_match_id}")
        return existing_match_id

    def _register_to_match(self, match_id):
        match = self.matches[match_id]
        match.register_player()
        logger.log(f"{UrlInfo.get_param('player')} registered to match {match_id}")
        match.wait_for_players()

    def _response_invalid_input(self, player_name, action):
        match_id = UrlInfo.get_param('new_match_id') if action == "host" else UrlInfo.get_param('existing_match_id')

        invalid_match_id = action == "host" and (not match_id or match_id in self.matches)
        invalid_player_name = not player_name or player_name == "Moti" or \
            (action == "join" and player_name in self.matches[match_id].players)

        return invalid_match_id, invalid_player_name

    def _response_game_over(self):
        match = self.matches[UrlInfo.get_param('match')]

        if match.phase_b.winners:
            return render_template("game_over.html", **match.context_game_over(reason="all_eliminated"))

        if UrlInfo.get_param('player') not in match.players:
            return render_template("game_over.html", **match.context_game_over(reason="player_eliminated"))

    @staticmethod
    def open_game_window():
        return render_template("open_window.html")

    def welcome(self):
        available_matches = [match_id for match_id in self.matches if self.matches[match_id].accept_new_players]

        if request.method == "GET":
            return render_template("/intro/welcome.html", title="Welcome",
                                   base_url=UrlInfo.get_base_url(), available_matches=available_matches)

        if request.method == "POST":
            player_name = UrlInfo.get_param('player')
            action = UrlInfo.get_param('action')

            invalid_input = self._response_invalid_input(player_name, action)
            if invalid_input[0] or invalid_input[1]:
                return render_template("/intro/welcome.html", title="Welcome",
                                       base_url=UrlInfo.get_base_url(), available_matches=available_matches,
                                       invalid_match_id=invalid_input[0], invalid_name=invalid_input[1])

            match_id = self._new_match() if action == "host" else self._existing_match() if action == "join" else None
            self._register_to_match(match_id)
            return redirect(self._build_url(f"assign_turn?player={player_name}&match={match_id}"))

    def rematch(self):
        match_id = UrlInfo.get_param("match")
        match = self.matches[match_id]

        match.clear_players()
        match.clear_player_turns()

        self._register_to_match(match_id)

        return render_template("/intro/assign_turn.html", **match.context_intro())

    def end_match(self):
        match_id = UrlInfo.get_param("match")
        if match_id in self.matches:
            del self.matches[match_id]
        return redirect(self._build_url("fakau"))

    def assign_turn(self):
        match_id = UrlInfo.get_param("match")
        match = self.matches[match_id]
        match.accept_new_players = False

        if request.method == "GET":
            return render_template("/intro/assign_turn.html", **match.context_intro())

        if request.method == "POST":
            match.assign_player_turn()
            logger.log(f"{UrlInfo.get_param('player')} was assigned a turn")

            match.initiate_phases()
            logger.log(f"Match {match_id} initiated:\n"
                       f"Players: {[player_name for player_name in match.players]}\n"
                       f"Turns: {match.player_turns}\n"
                       f"Controlling: {match.controlling_player}")
            return self.new_round()

    def new_round(self):
        match = self.matches[UrlInfo.get_param("match")]
        match.phase_a.reset_phase(match=match)

        if match.phase_a.is_player_turn(UrlInfo.get_param('player')):
            logger.log(f"{UrlInfo.get_param('player')} is drawing cards")
            return render_template("/phase_a/draw_cards.html", **match.phase_a.context_draw_cards())

        elif match.phase_a.is_player_turn("Moti"):
            logger.log("Moti's turn")
            match.phase_a.play_moti_turn(match=match)

        return self._wait_for_statement()

    def make_statement(self):
        match = self.matches[UrlInfo.get_param('match')]
        player_name = UrlInfo.get_param('player')

        if request.method == "GET":
            match.players[player_name].draw_cards(match.phase_a.deck)

            if match.players[player_name].cannot_make_statement():
                logger.log(f"{player_name} cannot make a statement")
                return render_template("game_over.html", **match.context_game_over(reason='cannot_state'))

            else:
                logger.log(f"{UrlInfo.get_param('player')} is making a statement")
                return render_template("phase_a/make_statement.html",
                                       **match.phase_a.context_make_statement())

        if request.method == "POST":
            match.phase_a.handle_player_statement(request.form.to_dict())
            logger.log(f"{UrlInfo.get_param('player')}'s statement: {match.phase_a.statement}")

            match.wait_for_players()
            match.phase_a.calc_results()

            if match.players[match.phase_a.player_making_statement].is_holding_jc():
                return render_template("game_over.html", **match.context_game_over(reason="jc_drawn"))

            else:
                return render_template("phase_a/show_results.html", **match.phase_a.context_show_results())

    def make_response(self):
        match = self.matches[UrlInfo.get_param('match')]

        if "response" not in request.args.to_dict():
            logger.log(f"{UrlInfo.get_param('player')} is making a response")
            return render_template("phase_a/make_response.html", **match.phase_a.context_make_response())

        else:
            player_response = request.args.to_dict()["response"]
            match.phase_a.add_player_response(UrlInfo.get_param('player'), player_response)
            logger.log(f"{UrlInfo.get_param('player')}'s response: {bool(int(player_response))}")
            match.wait_for_players()

            match.phase_a.calc_results()

            if match.players[match.phase_a.player_making_statement].is_holding_jc():
                return render_template("game_over.html", **match.context_game_over(reason="jc_drawn"))

            else:
                return render_template("phase_a/show_results.html", **match.phase_a.context_show_results())

    def lay_down_cards(self):
        match = self.matches[UrlInfo.get_param('match')]

        if request.method == "GET":
            match.wait_for_players()
            match.eliminate_players()
            game_over_response = self._response_game_over()
            if game_over_response:
                logger.log(f"Game is over for {UrlInfo.get_param('player')}")
                return game_over_response

            else:
                match.phase_b.reset_phase(match=match)
                logger.log(f"{UrlInfo.get_param('player')} needs to lay down cards")
                return render_template("phase_b/lay_down_cards.html", **match.phase_b.context_lay_down_cards())

        if request.method == "POST":
            logger.log(f"{UrlInfo.get_param('player')} has lay down cards")
            match.phase_b.moti_lay_down_cards(match=match)
            match.phase_b.lay_down_player_cards(match=match)
            logger.log(f"{UrlInfo.get_param('player')} needs to declare cards")
            return render_template("/phase_b/declare_cards.html", **match.phase_b.context_declare_cards())

    def reveal_cards(self):
        match = self.matches[UrlInfo.get_param('match')]

        match.phase_b.use_player_credits()
        match.wait_for_players()

        match.phase_b.reveal_cards(match=match)

        if match.phase_b.winners and len(match.players) > 1:
            return render_template("game_over.html", **match.context_game_over(reason="jc_revealed"))

        logger.log(f"{UrlInfo.get_param('player')} is shown revealed cards")
        return render_template("phase_b/reveal_cards.html", **match.phase_b.context_reveal_cards(match=match))

    def shortcut(self):
        if not UrlInfo.get_param("match"):
            return redirect("/shortcut?match=debug&player=Omri")

        if "debug" not in self.matches:
            self.matches["debug"] = Match(2)
            self.matches["debug"].shortcut_phase_b()

        return render_template("phase_b/lay_down_cards.html", **self.matches["debug"].phase_b.context_lay_down_cards())
