from src.phase import Phase
from src.card import special_cards
from src.helpers import UrlInfo, logger, performed_once

from flask import request
from random import choice


class PhaseB(Phase):
    def __init__(self, players):
        super().__init__(players)
        self.credit_dict = dict()
        self.declared_indexes = dict()
        self.winners = []
        self.losers = []
        self.alert_message = None

    @staticmethod
    def _create_declared_for_context(declare_indexes):
        simple_declared_indexes = dict()

        for player_name in declare_indexes:
            simple_declared_indexes[player_name] = ""

            for other_player_name in declare_indexes[player_name]:
                simple_declared_indexes[player_name] += declare_indexes[player_name][other_player_name]

            simple_declared_indexes[player_name] = "".join(set(list(simple_declared_indexes[player_name])))
        return simple_declared_indexes

    def _find_players_card_diff(self, player_name, other_player_name):
        return len(self.players[player_name].face_down_cards) - \
               len(self.players[other_player_name].face_down_cards)

    def _calc_credit(self):
        for player_name in self.players:
            for other_player_name in self.players:
                card_diff = self._find_players_card_diff(player_name, other_player_name)
                if card_diff > 0:
                    self.credit_dict[f"{player_name}_{other_player_name}"] = card_diff

    def _find_winner(self):
        def is_win_stolen():
            if player_name in self.declared_indexes:
                for declaring_player in self.declared_indexes[player_name]:
                    if jack_index in self.declared_indexes[player_name][declaring_player]:
                        self.winners = [declaring_player]
                        return True
            return False

        def is_jh_revealed():
            if special_cards.JACK_HEARTS in self.players[player_name].face_down_cards:
                self.winners = [name for name in self.players if name != player_name]

        for player_name in self.players:
            if special_cards.JACK_CLUBS in self.players[player_name].face_down_cards:
                self.winners = [player_name]
                jack_index = str(self.players[player_name].face_down_cards.index(special_cards.JACK_CLUBS))

                if not is_win_stolen():
                    is_jh_revealed()

    def _pay_punishments(self):
        for player_name in self.players:
            for other_player_name in self.players:
                if self.players[player_name].is_busted() and not self.players[other_player_name].is_busted():
                    card_diff = abs(self._find_players_card_diff(player_name, other_player_name))
                    punishment = 2 * card_diff if card_diff > 0 else 1
                    self.players[other_player_name].earn_cards(self.players[player_name].deck, punishment)
                    logger.log(f"{other_player_name} takes {punishment} cards from {player_name} (punishment)")

    def _moti_use_credit(self):
        if "Moti" in self.players:
            moti_declare_dict = self.players["Moti"].use_credit(self.players, self.credit_dict)
            logger.log(f"Moti declares: {moti_declare_dict}")

            for player_name in moti_declare_dict:
                if player_name not in self.declared_indexes:
                    self.declared_indexes[player_name] = dict()
                self.declared_indexes[player_name]["Moti"] = moti_declare_dict[player_name]

    def _unhide_players_cards(self):
        for player_name in self.players:
            self.players[player_name].reveal_face_down_cards()

    @performed_once
    def _find_losing_players(self):
        self.losers = [player_name for player_name in self.players if len(self.players[player_name].deck) == 0]
        alert_message_dict = {player_name: "was eliminated" for player_name in self.losers}
        self._inherit_moti(alert_message_dict)
        self._set_alert_message(alert_message_dict)

    def _inherit_moti(self, alert_message_dict):
        def is_game_finished():
            return len(self.players) - len(self.losers) < 2

        if "Moti" not in self.players:
            return

        if self.losers and "Moti" not in self.losers and not is_game_finished():
            inheriting_player = choice(self.losers)
            logger.log(f"{inheriting_player} inherits Moti")

            self.players[inheriting_player].deck = [card for card in self.players["Moti"].deck]
            self.players["Moti"].deck = []
            self.losers.pop(self.losers.index(inheriting_player))
            self.losers.append("Moti")

            alert_message_dict[inheriting_player] += ", but inherited Moti"

    def _set_alert_message(self, alert_message_dict):
        if alert_message_dict:
            self.alert_message = ""
            for player_name in alert_message_dict:
                self.alert_message += f"{player_name} {alert_message_dict[player_name]}!\\n"

    def context_lay_down_cards(self):
        return self._create_context(title="Lay down cards")

    def context_declare_cards(self):
        self._calc_credit()
        return self._create_context(title="Declare cards",
                                    credit=self.credit_dict)

    def context_reveal_cards(self, **kwargs):
        simple_declared = self._create_declared_for_context(self.declared_indexes)
        player_sums = {player_name: self.players[player_name].calc_sum_of_cards() for player_name in self.players}

        self._find_losing_players(**kwargs)

        return self._create_context(title="Reveal cards",
                                    player_sums=player_sums,
                                    declared_indexes=simple_declared,
                                    alert_message=self.alert_message)

    @performed_once
    def moti_lay_down_cards(self):
        if "Moti" in self.players:
            moti_num_cards = self.players["Moti"].lay_down_cards()
            logger.log(f"Moti lays down {moti_num_cards} cards.")

    def lay_down_player_cards(self, match):
        active_player = UrlInfo.get_param('player')
        num_of_cards_str = request.form.to_dict()["num_of_cards"]

        num_face_down_cards = self.players[active_player].lay_down_cards(int(num_of_cards_str))
        logger.log(f"{active_player} lays down {num_face_down_cards} cards.")
        match.wait_for_players()

    def use_player_credits(self):
        request_params = request.args.to_dict()
        active_player = UrlInfo.get_param('player')

        for player_name in self.players:
            if f"{active_player}_{player_name}" in self.credit_dict:
                take_num = self.credit_dict[f"{active_player}_{player_name}"]

                if f"declare_{player_name}" in request_params:
                    declared_indexes = request_params[f"declare_{player_name}"]
                    take_num -= len(declared_indexes)

                    if player_name not in self.declared_indexes:
                        self.declared_indexes[player_name] = dict()
                    self.declared_indexes[player_name][active_player] = declared_indexes

                logger.log(f"{active_player} takes {take_num} from {player_name} (credit)")
                self.players[active_player].earn_cards(self.players[player_name].deck, take_num)

    @performed_once
    def reveal_cards(self):
        self._unhide_players_cards()
        self._moti_use_credit()
        self._find_winner()
        self._pay_punishments()

    @performed_once
    def reset_phase(self):
        self.credit_dict = dict()
        self.declared_indexes = dict()
        self.losers = []
        self.alert_message = None

        for player_name in self.players:
            self.players[player_name].clear_face_down_cards()
