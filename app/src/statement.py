from src.card import unicode_suit
from random import choice


class Statement:
    def __init__(self):
        self.statements = {"color": ["red", "black"],
                           "higher": range(1, 14),
                           "lower": range(1, 14),
                           "suit": ["heart", "diamond", "spade", "club"]}

        self.jc_statement = {"the": "11_c"}

    @staticmethod
    def _create_card_statement(card, quality, value):
        statement_dict = {"color": f"{card} is {value}",
                          "suit": f"{card}'s suit is {unicode_suit.str_to_chr(value)}",
                          "higher": f"{card} is {quality} than {value}",
                          "lower": f"{card} is {quality} than {value}"}

        return statement_dict[quality]

    def create_random_statement(self):
        statement = dict()

        for i in range(2):
            random_quality = choice(list(self.statements.keys()))
            random_value = choice(self.statements[random_quality])
            statement[f"card{i+1}"] = {random_quality: random_value}

        return statement

    def create_jc_statement(self):
        return {"card1": self.jc_statement, "card2": {}}

    def is_statement_true(self, statement: dict, drawn_cards: list):
        card1 = drawn_cards[0]
        card2 = drawn_cards[1]

        condition1 = statement["card1"]
        condition2 = statement["card2"]

        if condition1 == self.jc_statement:
            return card1.is_match_condition(condition1) or card2.is_match_condition(condition1)

        bool1 = card1.is_match_condition(condition1) and card2.is_match_condition(condition2)
        bool2 = card1.is_match_condition(condition2) and card2.is_match_condition(condition1)

        return bool1 or bool2

    def create_options_for_player(self):
        options_for_player = []

        for option in self.statements:
            if option == "color":
                options_for_player.extend(self.statements[option])

            elif option == "suit":
                for suit in self.statements[option]:
                    options_for_player.append(unicode_suit.str_to_chr(suit))

            else:
                options_for_player.append(option)

        return options_for_player

    def read_player_statement(self, statement):
        if not statement:
            return self.create_jc_statement()

        dict_statement = dict()

        for num in ["1", "2"]:
            if statement[f"card{num}"] in ["red", "black"]:
                dict_statement[f"card{num}"] = {"color": statement[f"card{num}"]}

            elif statement[f"card{num}"] in ["higher", "lower"]:
                dict_statement[f"card{num}"] = {statement[f"card{num}"]: int(statement[f"value{num}"])}

            else:
                dict_statement[f"card{num}"] = {"suit": unicode_suit.chr_to_str(statement[f"card{num}"])}

        return dict_statement

    def display_statement(self, statement: dict):
        statement_str = ""

        for card in statement:
            if statement[card] == self.jc_statement:
                return f"I am holding the J{unicode_suit.CLUB}"

            quality = list(statement[card].keys())[0]
            value = statement[card][quality]
            statement_str += f"{self._create_card_statement(card, quality, value)}\n"

        statement_str = statement_str[:-1].replace('\n', ' and ')
        statement_str = statement_str.replace("card1", "One card")
        statement_str = statement_str.replace("card2", "the other")

        return statement_str
