from src.helpers import UrlInfo


class Phase:
    def __init__(self, players):
        self.players = players

    def _create_context(self, **kwargs):
        context = {"players": self.players,
                   "active_player": UrlInfo.get_param('player'),
                   "base_url": UrlInfo.get_base_url(),
                   "match": UrlInfo.get_param('match')}
        context.update(kwargs)
        return context
