from src.player import Player, MotiPlayer
from src.phase_a import PhaseA
from src.phase_b import PhaseB
from src.helpers import UrlInfo, waiting, logger, performed_once
from src.card import create_cards_html, unicode_suit, special_cards
from random import randint


class Match:
    def __init__(self, num_of_players):
        self.accept_new_players = True
        self.num_of_players = num_of_players
        self.controlling_player = ""
        self.waiting_players = []

        self.players = {"Moti": MotiPlayer()}
        self.player_turns = {randint(2, self.num_of_players + 1): "Moti"}
        self.phase_a = None
        self.phase_b = None
        self.total_score = dict()

    def _set_controlling_player(self):
        if not self.controlling_player:
            self.controlling_player = self.player_turns[1]

    def _adjust_waiting_players(self):
        if self.waiting_players:
            self.waiting_players.pop()
            self.num_of_players -= 1

    def _remove_players(self):
        for losing_player in self.phase_b.losers:
            if losing_player != "Moti":
                self._adjust_waiting_players()

            del self.players[losing_player]

    def _update_values(self):
        self.phase_b.winners = list(self.players.keys()) if len(self.players) == 1 \
            else ["Moti"] if len(self.players) == 0 else []

        for player_name in self.players:
            if player_name != "Moti":
                self.controlling_player = player_name

    def _find_jc_holder(self):
        for player_name in self.players:
            player_cards = self.players[player_name].face_down_cards
            if special_cards.JACK_CLUBS in player_cards:
                return self.players[player_name]

    @performed_once
    def _update_total_score(self, context):
        for player_name in self.players:
            if player_name not in self.total_score:
                self.total_score[player_name] = 0

            if player_name in context["winners"]:
                self.total_score[player_name] += 1

    def _create_total_score_message(self):
        total_score_message = "===== Match Summary =====\\n\\n"
        for player_name in self.total_score:
            total_score_message += f"{player_name}: {self.total_score[player_name]},"
        return total_score_message[:-1]

    def _context_cannot_statement(self):
        winning_players = [player_name for player_name in self.players
                           if player_name != self.phase_a.player_making_statement]
        drawn_cards = self.phase_a.players[self.phase_a.player_making_statement].drawn_cards

        return {"winners": winning_players,
                "message": f"{self.phase_a.player_making_statement} Could not make a statement!",
                "cards": create_cards_html(drawn_cards),
                "cards_title": f"{self.phase_a.player_making_statement}'s drawn cards"}

    def _context_all_eliminated(self):
        winning_players = self.phase_b.winners
        return {"winners": winning_players,
                "message": "All other player were eliminated.",
                "cards": create_cards_html(self.players[winning_players[0]].deck),
                "cards_title": "Remaining cards are"}

    @staticmethod
    def _context_player_eliminated():
        return {"message": "Sadly you were eliminated.... :("}

    def _context_jc_drawn(self):
        stating_player = self.players[self.phase_a.player_making_statement]
        return {"winners": self.phase_a.results.keys(),
                "cards_title": f"{stating_player.name}'s drawn cards",
                "cards": create_cards_html(stating_player.drawn_cards)}

    def _context_jc_revealed(self):
        winner_name = self.phase_b.winners[0]
        jc_holder = self._find_jc_holder()

        message = f"Revealed the J{unicode_suit.CLUB}!" if jc_holder.name == winner_name else \
            "Win was lost beacuse of NACHS!" if special_cards.JACK_HEARTS in jc_holder.face_down_cards else \
            f"Declared the J{unicode_suit.CLUB}!"

        return {"winners": self.phase_b.winners,
                "message": message,
                "cards_title": f"{jc_holder.name} revealed",
                "cards": create_cards_html(jc_holder.face_down_cards)}

    def _reason_context(self, reason):
        context_by_reason = {"cannot_state": self._context_cannot_statement,
                             "all_eliminated": self._context_all_eliminated,
                             "player_eliminated": self._context_player_eliminated,
                             "jc_drawn": self._context_jc_drawn,
                             "jc_revealed": self._context_jc_revealed}
        return context_by_reason[reason]()

    def is_all_players_waiting(self):
        return len(self.waiting_players) == self.num_of_players

    @performed_once
    def clear_players(self):
        self.players = {"Moti": MotiPlayer()}

    def clear_waiting_players(self):
        waiting.wait()
        self.waiting_players.clear()
        waiting.wait()

    @performed_once
    def clear_player_turns(self):
        self.player_turns = {randint(2, self.num_of_players + 1): "Moti"}

    def wait_for_players(self):
        self.waiting_players.append(UrlInfo.get_param('player'))
        waiting.wait_for_event(self.is_all_players_waiting)
        self.clear_waiting_players()

    def context_intro(self):
        return {"title": "Assign turn",
                "players": self.players,
                "active_player": UrlInfo.get_param('player'),
                "base_url": UrlInfo.get_base_url(),
                "match": UrlInfo.get_param("match")}

    def context_game_over(self, reason):
        context = {"active_player": UrlInfo.get_param('player'),
                   "base_url": UrlInfo.get_base_url(),
                   "match": UrlInfo.get_param("match")}
        context.update(self._reason_context(reason))

        self._update_total_score(context)
        context["total_score_message"] = self._create_total_score_message()
        return context

    def register_player(self):
        player_name = UrlInfo.get_param('player')
        self.players[player_name] = Player(player_name)

    def assign_player_turn(self):
        for turn in range(1, self.num_of_players + 2):
            if turn not in self.player_turns:
                self.player_turns[turn] = UrlInfo.get_param('player')
                break

        self._set_controlling_player()
        self.wait_for_players()

    @performed_once
    def initiate_phases(self):
        self.phase_a = PhaseA(self.players, player_turns=self.player_turns)
        self.phase_b = PhaseB(self.players)

    @performed_once
    def eliminate_players(self):
        waiting.wait()
        self._remove_players()
        self._update_values()
        logger.log(f"Players left: {[player for player in self.players]}")

    def shortcut_phase_b(self):
        from src.card import special_cards

        self.players = {"Omri": Player("Omri"), "Moti": MotiPlayer()}
        self.num_of_players = 1
        self.controlling_player = "Omri"
        self.waiting_players = []
        self.player_turns = {randint(2, self.num_of_players + 1): "Moti"}
        self.phase_b = PhaseB(self.players)

        self.players["Omri"].deck = [special_cards.JACK_HEARTS for i in range(2)]
        self.players["Moti"].deck = [special_cards.JACK_HEARTS for i in range(20)]
