class Card:
    def __init__(self, value: int, suit: str):
        self.value = value
        self.suit = suit
        self.color = "red" if suit in ["d", "h"] else "black"
        self.is_hidden = False

    def __eq__(self, other):
        return self.suit == other.suit and self.value == other.value

    def _is_higher(self, value: int):
        return self.value > value

    def _is_lower(self, value: int):
        return self.value < value

    def _is_color(self, value: str):
        return self.color == value

    def _is_suit(self, value: str):
        return self.suit == value[0]

    def _is_the(self, value_suit: str):
        value, suit = value_suit.split('_')
        return self.value == int(value) and self.suit == suit

    def create_image_path(self):
        if self.is_hidden:
            image = "gray_back"

        else:
            special_values = {1: "A", 11: "J", 12: "Q", 13: "K"}
            card_value = special_values[self.value] if self.value in special_values else self.value
            card_suit = self.suit.upper()
            image = f"{card_value}{card_suit}"

        return f"/static/img/cards/{image}.png"

    def is_match_condition(self, condition: dict):
        condition_method_dict = {"higher": self._is_higher,
                                 "lower": self._is_lower,
                                 "color": self._is_color,
                                 "suit": self._is_suit,
                                 "the": self._is_the}

        condition_key = list(condition.keys())[0]
        condition_value = condition[condition_key]

        return condition_method_dict[condition_key](condition_value)

    def hide(self):
        self.is_hidden = True

    def unhide(self):
        self.is_hidden = False


class SpecialCards:
    def __init__(self):
        self.JACK_CLUBS = Card(11, "c")
        self.JACK_HEARTS = Card(11, "h")


class UnicodeSuit:
    def __init__(self):
        self.suit_dict = {"heart": 9829, "diamond": 9830, "spade": 9824, "club": 9827}
        self.CLUB = self.str_to_chr("club")

    def str_to_chr(self, suit):
        if suit in self.suit_dict:
            return chr(self.suit_dict[suit])

    def chr_to_str(self, suit):
        for suit_str in self.suit_dict:
            if self.suit_dict[suit_str] == ord(suit):
                return suit_str


def create_cards_html(cards: list):
    return [card.create_image_path() for card in cards]


special_cards = SpecialCards()
unicode_suit = UnicodeSuit()
