from src.phase import Phase
from src.card import Card, unicode_suit, create_cards_html
from src.statement import Statement
from src.helpers import UrlInfo, performed_once


class PhaseA(Phase):
    def __init__(self, players, player_turns):
        super().__init__(players)
        self.st = Statement()
        self.deck = [Card(value, suit) for value in range(1, 14) for suit in ["h", "d", "s", "c"]]
        self.earn_cards = self._calc_num_earn_cards()
        self.player_turns = player_turns

        self.player_making_statement = None
        self.statement = None
        self.responses = dict()
        self.results = None

    def _calc_num_earn_cards(self):
        num_of_cards = 7 - len(self.players)

        if num_of_cards % 2 != 0:
            num_of_cards += 1

        return max(2, num_of_cards)

    def _create_phase_a_context(self, **kwargs):
        context = {"deck": self.deck,
                   "cards": create_cards_html(self.players[self.player_making_statement].drawn_cards)}
        context.update(kwargs)
        return self._create_context(**context)

    def _hide_statement_cards(self):
        for statement_card in self.players[self.player_making_statement].drawn_cards:
            statement_card.hide()

    def _unhide_statement_cards(self):
        for statement_card in self.players[self.player_making_statement].drawn_cards:
            statement_card.unhide()

    def _check_statement_bool(self):
        return self.st.is_statement_true(self.statement, self.players[self.player_making_statement].drawn_cards)

    def calc_results(self):
        self._unhide_statement_cards()
        if self.results is not None:
            return

        self.results = dict()
        statement_bool = self._check_statement_bool()

        for player_name in self.responses:
            winning_player_name = player_name if self.responses[player_name] == statement_bool \
                else self.player_making_statement

            if winning_player_name not in self.results:
                self.results[winning_player_name] = 0

            earn_cards = 2 * self.earn_cards if self.statement == self.st.create_jc_statement() else self.earn_cards
            self.results[winning_player_name] += self.players[winning_player_name].earn_cards(self.deck, earn_cards)

    def find_next_player(self):
        if self.player_making_statement is None:
            return self.player_turns[1]

        for turn in self.player_turns:
            if self.player_turns[turn] == self.player_making_statement:
                next_turn = (turn + 1) if (turn + 1) in self.player_turns else 1
                return self.player_turns[next_turn]

    def is_player_turn(self, player_name):
        return self.player_making_statement == player_name

    def is_cards_drawn(self):
        return self.players[self.player_making_statement].drawn_cards != []

    def is_statement(self):
        return self.statement is not None

    def context_draw_cards(self):
        return self._create_phase_a_context(title="Draw cards")

    def context_make_statement(self):
        player_name = UrlInfo.get_param('player')
        options_for_player = None if self.players[player_name].is_holding_jc() else self.st.create_options_for_player()

        return self._create_phase_a_context(title="Make statement",
                                            jc_card=f"J{unicode_suit.CLUB}",
                                            options=options_for_player)

    def context_make_response(self):
        self._hide_statement_cards()
        return self._create_phase_a_context(title="Respond",
                                            statement=self.st.display_statement(self.statement),
                                            player_making_statement=self.player_making_statement)

    def context_show_results(self):
        return self._create_phase_a_context(title="Results",
                                            player_making_statement=self.player_making_statement,
                                            statement=self.st.display_statement(self.statement),
                                            statement_bool=str(self._check_statement_bool()),
                                            responses=self.responses,
                                            results=self.results)

    def add_player_response(self, player_name, response):
        self.responses[player_name] = bool(int(response))

    def handle_player_statement(self, statement):
        self.statement = self.st.read_player_statement(statement)
        self.responses["Moti"] = self.players["Moti"].respond()

        if self.statement == self.st.create_jc_statement():
            self.players[self.player_making_statement].holding_jc_count += 1

    @performed_once
    def play_moti_turn(self):
        moti = self.players["Moti"]
        moti.draw_cards(self.deck)
        self.statement = moti.make_statement()

    @performed_once
    def reset_phase(self):
        self.player_making_statement = self.find_next_player()
        self.players[self.player_making_statement].clear_drawn_cards()

        self.statement = None
        self.responses = dict()
        self.results = None
