function getPlayerDeclaredIndexes(player_name){
    var declared_indexes = "" ;

    for (var index=0; index<5; index++){
    var card_id = player_name + '_' + index.toString()
    var card = document.getElementById(card_id) ;

    if (card != null)
        if (isCardDeclared(card))
            declared_indexes += index.toString() ;
    }

    return declared_indexes ;
}


function countDeclared(player_name){
    var player_declared_indexes = getPlayerDeclaredIndexes(player_name);
    return player_declared_indexes.length;
}


function isDeclareAllowed(player_name, credit){
    var declared = countDeclared(player_name);
    return declared < credit ;
}


function cardClicked(card_id, credit){
    var card = document.getElementById(card_id) ;
    var player_name = card_id.split('_')[0] ;

    if (isCardDeclared(card))
        undeclareCard(card);

    else if (isDeclareAllowed(player_name, credit))
        declareCard(card);

    else
        alert('You cannot declare more than ' + credit.toString() + ' cards from ' + player_name) ;
}


function isCardDeclared(card){
    return card.style.backgroundColor == "red";
}


function declareCard(card){
    card.style.backgroundColor = "red";
}


function undeclareCard(card){
    card.style.backgroundColor = "whitesmoke";
}


function revealCards(player_names, active_player, match){
    var reveal_url = 'reveal_cards?player=' + active_player + '&match=' + match;

    for (var index in player_names){
        var name = player_names[index];
        var declared_indexes = getPlayerDeclaredIndexes(name)

        if (declared_indexes.length != 0)
            reveal_url += '&declare_' + name + '=' + declared_indexes ;
    }

    clickableCards = document.getElementsByTagName("a");
    for (var index in clickableCards){
        clickableCards[index].onclick = "" ;
    }

    buttonClicked('GET', reveal_url);
}
