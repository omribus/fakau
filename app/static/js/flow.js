function getUrl(serverRequest){
    serverRequest.send();
}


function postUrl(serverRequest, data){
    serverRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    data = data == undefined ? "" : data ;
    serverRequest.send(data);
}


function asyncRequestToServer(method, url, data){
    var serverRequest = new XMLHttpRequest();
    var fullUrl = window.location.origin + '/' + url;

    serverRequest.onreadystatechange = function() {
        if (serverRequest.readyState == 4)
            document.getElementById("dynamic_div").innerHTML = this.responseText ;
    };

    serverRequest.open(method, fullUrl, true);
    method == "POST" ? postUrl(serverRequest, data) : getUrl(serverRequest) ;
}


function disableAllInputs(){
    var buttons = document.getElementsByTagName('button');
    for (var i = 0; i < buttons.length; i++) {
        buttons[i].disabled = true;
    }

    var inputs = document.getElementsByTagName('input');
    for (var i = 0; i < inputs.length; i++) {
        inputs[i].disabled = true;
    }
}


function createInputDataString(){
    var inputDataString = ""

    var selectData = document.getElementsByTagName('select');
    for (var i = 0; i < selectData.length ; i++){
        inputDataString += selectData[i].name + "=" + selectData[i].value + "&" ;
    }

    var inputData = document.getElementsByTagName('input');
    for (var i = 0; i < inputData.length ; i++){
        inputDataString += inputData[i].name + "=" + inputData[i].value + "&" ;
    }

    return inputDataString;
}


function buttonClicked(method, url, data){
    disableAllInputs();
    document.getElementById('waiting').style = "display: inline";
    asyncRequestToServer(method, url, data) ;
}
