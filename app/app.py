from flask import Flask
from src.server import Server


app = Flask(__name__)
server = Server()


@app.route("/")
def open_game_window():
    return server.open_game_window()


@app.route("/fakau", methods=["GET", "POST"])
def welcome():
    return server.welcome()


@app.route("/rematch")
def rematch():
    return server.rematch()


@app.route("/end_match")
def end_match():
    return server.end_match()


@app.route("/assign_turn", methods=["GET", "POST"])
def assign_turn():
    return server.assign_turn()


@app.route('/new_round', methods=["GET", "POST"])
def new_round():
    return server.new_round()


@app.route('/make_statement', methods=["GET", "POST"])
def make_statement():
    return server.make_statement()


@app.route('/make_response')
def make_response():
    return server.make_response()


@app.route('/lay_down_cards', methods=["GET", "POST"])
def lay_down_cards():
    return server.lay_down_cards()


@app.route("/reveal_cards")
def reveal_cards():
    return server.reveal_cards()


@app.route("/shortcut")
def shortcut():
    return server.shortcut()


if __name__ == "__main__":
    app.run(host='0.0.0.0')
