#!/bin/bash

echo -e "\n\n\n===== Building docker image =====\n"
sudo docker build -t fakau_server .

echo -e "\n\n\n===== Running docker image =====\n"
sudo docker stop fakau_server
sudo docker rm fakau_server
sudo docker run -d -p 5000:5000 --name fakau_server fakau_server

